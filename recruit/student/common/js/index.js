(function($) {
    $(function() {
        $(".slider").slick({
			slidesToShow: 1,
			fade: true,
			arrows: false,
			dots:false,
			speed: 500,
			autoplay:true,
			autoplaySpeed:5000,
			swipe:false
        });
    });
})(jQuery);
