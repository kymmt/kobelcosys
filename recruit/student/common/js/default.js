//pagetop

$(function() {
    $("a[href^=#]").click(function() {
        var speed = 100;
        var href = $(this).attr("href");
        var target = $(href == "#" || href == "" ? "html" : href);
        var position = target.offset().top;
        $("body,html").animate({ scrollTop: position }, speed, "swing");
        return false;
    });
});

//responsive

$(function() {
    var $setElem = $(".switch img"),
        pcName = "_pc",
        spName = "_sp",
        replaceWidth = 750;

    $setElem.each(function() {
        var $this = $(this);
        function imgSize() {
            var windowWidth = parseInt($(window).width());
            if (windowWidth >= replaceWidth) {
                $this.attr("src", $this.attr("src").replace(spName, pcName)).css({ visibility: "visible" });
            } else if (windowWidth < replaceWidth) {
                $this.attr("src", $this.attr("src").replace(pcName, spName)).css({ visibility: "visible" });
            }
        }
        $(window).resize(function() {
            imgSize();
        });
        imgSize();
    });
});

//rollover

$(function() {
    if (
        navigator.userAgent.indexOf("iPhone") > 0 ||
        navigator.userAgent.indexOf("iPad") > 0 ||
        navigator.userAgent.indexOf("iPod") > 0 ||
        navigator.userAgent.indexOf("Android") > 0
    ) {
        function smartRollover() {
            if (document.getElementsByTagName) {
                var images = document.getElementsByTagName("img");
                for (var i = 0; i < images.length; i++) {
                    if (images[i].getAttribute("src").match("_off.")) {
                        images[i].ontouchstart = function() {
                            this.setAttribute("src", this.getAttribute("src").replace("_off.", "_on."));
                        };
                        images[i].ontouchmove = function() {
                            this.setAttribute("src", this.getAttribute("src").replace("_on.", "_off."));
                        };
                        images[i].ontouchend = function() {
                            this.setAttribute("src", this.getAttribute("src").replace("_on.", "_off."));
                        };
                    }
                }
            }
        }
        if (window.addEventListener) {
            window.addEventListener("load", smartRollover, false);
        } else if (window.attachEvent) {
            window.attachEvent("onload", smartRollover);
        }
    } else {
        function smartRollover() {
            if (document.getElementsByTagName) {
                var images = document.getElementsByTagName("img");
                for (var i = 0; i < images.length; i++) {
                    if (images[i].getAttribute("src").match("_off.")) {
                        images[i].onmouseover = function() {
                            this.setAttribute("src", this.getAttribute("src").replace("_off.", "_on."));
                        };
                        images[i].onmouseout = function() {
                            this.setAttribute("src", this.getAttribute("src").replace("_on.", "_off."));
                        };
                    }
                }
            }
        }
        if (window.addEventListener) {
            window.addEventListener("load", smartRollover, false);
        } else if (window.attachEvent) {
            window.attachEvent("onload", smartRollover);
        }
    }
});

//breadcrumb
$(function() {
    $("#header li a").after("&nbsp;&gt;");
});

//nav_height (PC)
$(function() {
    var mainHeight = $("#main").height();
    var navHeight = $("#nav").height();
    if ($(window).width() > 750) {
        if (mainHeight > navHeight) {
            $("#nav").height(mainHeight + 140);
        }
    }
});

//company_outline (PC)
$(function() {
    var c1height = $(".company_outline li.c1 dd").height();
    var c3height = $(".company_outline li.c3 dd").height();
    if ($(window).width() > 750) {
        $(".company_outline li.c2 dd").height(c1height);
        $(".company_outline li.c4 dd").height(c3height);
    }
});

//professionals (SP)
$(function() {
    $("div.professionals").before('<div id="sp_h1"></div>');
    $("#sp_h1").prepend($("#professionals_visual img").clone(true));
});

var mediaQuery = matchMedia("(max-width: 750px)");
handle(mediaQuery);
mediaQuery.addListener(handle);

/**
 * グローバルナビドロップダウン
 */
function handle(mq) {
    if (mq.matches) {
        $('[data-action="toggle-modal"]').off();
        $('[data-action="toggle-modal"] > a').on({
            click: function(e) {
                $(this).toggleClass("open");
                e.preventDefault();
                $(".sub-menu")
                    .stop()
                    .slideToggle();
                return false;
            }
        });
    } else {
        $('[data-action="toggle-modal"] > a').off();
        $('[data-action="toggle-modal"]').on({
            mouseenter: function() {
                $(".sub-menu")
                    .stop()
                    .delay(200)
                    .fadeIn();
            },
            mouseleave: function() {
                $(".sub-menu")
                    .stop()
                    .delay(200)
                    .fadeOut();
            }
        });
    }
}
/**
 *  SPメニュー開閉
 */

$(".sp-menu-button").on("click", function() {
    $(".menu-wrapper").addClass('is-open');

});
$(".sp-close-button").on("click", function() {
    $(".menu-wrapper").removeClass('is-open');
});
/**
 *  エントリーボタンモーダル
 */
$(function() {
    $(".entry-button").on("click", function() {
        $(".entry-modal").fadeIn();
    });
    $(".entry-modal-close").on("click", function() {
        $(".entry-modal").fadeOut();
    });
});
